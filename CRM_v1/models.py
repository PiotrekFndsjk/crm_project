# from flask_sqlalchemy import SQLAlchemy #to jest orm flaska dla sql
#
# db = SQLAlchemy()
#
# #tworzymy klasę, która dziedziczy po klasie flask sqlalcehmy, która już wszystko potrafi ;)
# #wpisy robimy poprzez instancje klasy. Primary key jest we flasku autoincrement i sam się generuje
# class Task(db.Model):
#     __tablename__ = "task"
#     id = db.Column(db.Integer, primary_key=True)
#     text = db.Column(db.String, nullable=False)
#
#     def __str__(self):
#         return self.text